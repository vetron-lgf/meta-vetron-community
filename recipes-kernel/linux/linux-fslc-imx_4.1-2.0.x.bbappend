# Copyright (C) 2015, 2017 O.S. Systems Software LTDA.
# Released under the MIT license (see COPYING.MIT for the terms)

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

inherit kernel


SRC_URI += " \
    file://imx6qdl-vetron.dtsi \
    file://imx6dl-magnum-example.dts \
    file://0001-mx6s-magnum-config-patched.patch \
    file://imx6s-vetron_defconfig \
"

COMPATIBLE_MACHINE = "(mx6|mx7|magnum-imx6|imx6-magnum-example)"

KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT_u-boot-fslc-vetron-mx6}"
