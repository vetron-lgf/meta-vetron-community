SUMMARY = "A console-only image that fully supports the IMX6S Magnum device \
hardware."

include recipes-core/images/core-image-minimal.bb

IMAGE_FEATURES += "splash"
IMAGE_BASENAME = "core-image"

LICENSE = "MIT"

