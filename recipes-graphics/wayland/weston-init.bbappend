FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

#SRC_URI += "file://weston.ini"

do_install_append() {
	install -d ${D}/${sysconfdir}/weston
	install -m755 ${WORKDIR}/weston.ini ${D}/${sysconfdir}/weston/weston.ini
}

